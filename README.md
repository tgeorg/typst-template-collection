# Typst Template Collection

This repo is a collection of different typst templates for various use cases.

## About Typst

Typst is a modern typesetting language. It can be seen as an alternative to LaTeX and Word. It excels in being easy to use and having modern features.

## Contributing

The whole idea of this repository is to gather contribution by members of ETH Zurich. So feel welcome to fork the repo and create a pull request, create issues or contribute in any other way.

Since this repo uses the MIT licence you can use all the templates for non-commercial and commercial purposes.

## How to add a template

You can fork the repo and create a new folder in which you add your template. Then you can create a pull request to merge it into this collection.

When adding a new template please include a README where you briefly explain the following points:

- What is the template for?

- Are there extra things which need to be installed? (e.g. a specific font) Please include detailed installation instructions if such things exist.
- What can be configured? (e.g. title, author, ...)

- What functions are being exposed? (e.g. theorem, definition, ...)

- What typst version was the template designed for?
