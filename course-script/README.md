# Course script

This template is for writing a book like course script. It is inspired by the script "Diskrete Mathematik" by Ueli Maurer.

## Usage

To use this template you need to install the font P052 from the font folder. Additionally I can recommend to use [typos](https://github.com/crate-ci/typos) for checking for typos. 

Otherwise you can simply watch or compile the `script.typ` file using the typst cli utility.

## Config

The template exposes the following arguments in its main function:

- uni: the name of the university
- department: the name of the department where the course is being taught
- title: the name of the course
- author: the name of the author or authors
- semester: the semester and year in which the course is being taught
- preface: optional, can be used to supply a preface

## Functions

The following functions are being exposed and can be used:

- chapter: this function is the preferred way to add content, it takes a title as a named argument
- example: a numbered example environment
- definition: a numbered definition environment, takes boxed: bool as argument which determines if the background should be a gray box
- preface: a function which should be used for the preface

This template has been created with typst version 0.11.0.
