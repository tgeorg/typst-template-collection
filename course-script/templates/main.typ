#import "@preview/hydra:0.4.0": hydra

#let script(
  uni: "ETH Zürich", department: "Department of Computer Science", title: "Formal Methods and Functional Programming", author: "Constantin Mussaeus", semester: "Spring 2024", preface: none, body
) = {

  // Text style
  set document(title: title, author: author, date: datetime.today())
  set text(size: 14pt, font: "P052")
  set page(paper: "a4", margin: (x: 2cm, y: 3cm))
  show link: underline

  // Footnote style
  set footnote.entry(separator: line(length: 45%, stroke: 1.3pt))

  // Title page
  [
    #set align(center)
    #text(size: 30pt, uni)\
    \
    #text(size: 30pt, department)

    #set align(horizon)
    #strong(text(size: 55pt, title))

    #set align(bottom)
    #text(size: 28pt, author)

    \

    #text(size: 28pt, semester)
  ]


  if preface != none {
    set par(justify: true, first-line-indent: 20pt)
    pagebreak(weak: true)
    v(3cm)
    strong(text(size: 25pt, heading(level: 1, "Preface")))
    v(2cm)

    preface
    
  }

  pagebreak(weak: true)

  // Content outline
  [
    #v(3cm)
    #strong(text(size: 25pt, heading(level: 1, "Contents", outlined: false)))
    #v(2cm)

    #show outline.entry.where(
      level: 1
    ): it => {
      v(20pt, weak: true)
      strong(it)
    }

    #set text(size: 14pt)
    #outline(title: none, indent: auto)
  ]

  pagebreak(weak: true)

  // Heading styles
  set heading(numbering: "1.1.1  ")
  show heading.where(level: 1): it => {
    set par(first-line-indent: 0pt)
    pagebreak(weak: true)
    v(4cm)
    strong(text(size: 28pt, [Chapter #context counter(heading).display("1")]))
    parbreak()
    strong(text(size: 38pt, it.body))
    v(1.5cm)
    
  }
  show heading.where(level: 2): it => {
    set text(size: 20pt)
    strong(it)
  }

  // Header style
  set page(header: context {
    let current = counter(page).get()
    let is-chapter-start = query(heading.where(level: 1)).any(m =>
    counter(page).at(m.location()) == current)
    // don't show on pages where a chapter starts
    if not is-chapter-start {
      set align(horizon)
      pad(bottom: -2pt, {
        place(left, strong(hydra(2)))
        place(right, strong([#counter(page).get().first()]))
      })
      line(length: 100%, stroke: 1.3pt)
    }
  })

  set page(margin: (y: 2.5cm))
  set par(justify: true, first-line-indent: 20pt)

  
  body
}

#let preface(body) = {
  set heading(offset: 1, outlined: false)
  body
}

#let exampleC = counter("example")

#let example(body) = {
  set par(first-line-indent: 0pt)
  exampleC.step()
  strong({
    [Example ]
    context counter(heading).get().first()
    [.]
    context exampleC.get().first()
  })
  h(8pt)
  body
}

#let definitionC = counter("definition")
#let definition(boxed: true, body) = {
  set par(first-line-indent: 0pt)
  definitionC.step()
  let content = {
    strong({
      [Definition ]
      context counter(heading).get().first()
      [.]
      context definitionC.get().first()
    })
    h(8pt)
    body
    }
  if boxed {
    box(inset: 6pt, fill: color.rgb("d9d9d9"), width: 100%, content)
  } else {
    content
  }
}

#let chapter(title: "Title", body) = [
  #counter(footnote).update(0)
  #exampleC.update(0)
  #definitionC.update(0)
  = #title
  #[
    #set heading(offset: 1)
    #body
  ]
]

