#import "templates/main.typ": script, chapter, example, definition

#show: script.with(
  uni: "ETH Zürich",
  department: "Department of Computer Science",
  title: "Compiler Design",
  author: "Max Mustermann",
  semester: "Spring 2024",
  preface: include "preface.typ",
)

#include "chapters/chapter1.typ"
